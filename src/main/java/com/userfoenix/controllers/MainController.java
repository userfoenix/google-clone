package com.userfoenix.controllers;

import com.userfoenix.services.lucene.SearchEngineIndexer;
import com.userfoenix.services.lucene.SearchEngineSearcher;
import com.userfoenix.domain.Page;
import com.userfoenix.services.crawler.Crawler;
import com.userfoenix.validator.IndexerForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.*;

@Controller
public class MainController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static String INDEX_DIRECTORY_PATH = "tmp";
    private final static int ITEMS_LIMIT = 10;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index_form(IndexerForm indexerForm) {
        return "indexer";
    }

    @PostMapping("/index")
    public String indexer(@Valid IndexerForm indexerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        indexerForm.setTaskId(1);
        logger.info(indexerForm.toString());
        if (bindingResult.hasErrors()) {
            return "indexer";
        }

        String url = indexerForm.getUrl();
        Integer deep = indexerForm.getDeep();
        int taskId = 0;

        try {
            SearchEngineIndexer indexer = new SearchEngineIndexer(INDEX_DIRECTORY_PATH);
            Crawler crawler = new Crawler(indexer, deep, 64);
            taskId = crawler.index(url);
            indexer.close();
            int processed = indexer.getCounter();
            logger.info("processed="+processed);
        }
        catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        redirectAttributes.addFlashAttribute("taskId", taskId);
        return "redirect:/";
    }

    @RequestMapping(value = "/search")
    public String search_results(@RequestParam(value = "q", defaultValue = "", required = false) String q, @RequestParam(value = "sort", defaultValue = "") String sort, Model model) {
        int found = 0;
        if (q.length()>0){
             try {
                SearchEngineSearcher searcher = new SearchEngineSearcher(INDEX_DIRECTORY_PATH);
                List<Page> items = searcher.search(q, ITEMS_LIMIT, 0, sort);
                found = items.size();
                model.addAttribute("items", items);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        model.addAttribute("q", q);
        model.addAttribute("found", found);
        model.addAttribute("sort", sort);
        model.addAttribute("limit", ITEMS_LIMIT);

        return "index";
    }

    @RequestMapping(value="/ax_search", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Page> ax_search(@RequestParam("q") String q, @RequestParam("offset") int offset, @RequestParam("sort") String sort) {
        if (q.length()==0){
            return new ArrayList<>();
        }
        SearchEngineSearcher searcher = null;
        try {
            searcher = new SearchEngineSearcher(INDEX_DIRECTORY_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searcher.search(q, ITEMS_LIMIT, offset, sort);
    }



}