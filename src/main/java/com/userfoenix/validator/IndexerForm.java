package com.userfoenix.validator;

import javax.validation.constraints.*;

public class IndexerForm {

    @NotNull
    @Pattern(regexp = "^https?:\\/\\/.+", message = "Invalid URL format!")
    private String url;

    @NotNull
    @Min(1)
    @Max(5)
    private Integer deep;

    private Integer taskId = 0;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getDeep() {
        return this.deep;
    }

    public void setDeep(Integer deep) {
        this.deep = deep;
    }

    public String toString() {
        return "Indexer(url: " + this.url + ", deep: " + this.deep + " , taskId: " + this.taskId + ")";
    }
}