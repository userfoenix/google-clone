package com.userfoenix.domain;

public class Page {
    private String url;
    private String title;
    private String content;

    public Page(){

    }

    public Page(String url, String title, String content){
        this.url = url;
        this.title = title;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }
}
