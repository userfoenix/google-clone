package com.userfoenix.services.lucene;

import java.io.IOException;
import java.nio.file.Paths;

import com.userfoenix.services.crawler.CrawlerCallback;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.document.*;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchEngineIndexer implements CrawlerCallback {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static StandardAnalyzer analyzer = new StandardAnalyzer();

    private IndexWriter writer;

    private int counter;

    public SearchEngineIndexer(String indexDirectoryPath) throws IOException {
        //this directory will contain the indexes
        Directory indexDirectory = FSDirectory.open(Paths.get(indexDirectoryPath));

        //create the indexer
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        writer = new IndexWriter(indexDirectory, config);

        // Processed pages count
        counter = 0;
    }

    public int getCounter() {
        return counter;
    }

    public void close() throws CorruptIndexException, IOException {
        writer.close();
    }

    public void process(String url, String title, String text) {
        // Good idea to append content into the text file(s) and process after, to increase crawling speed
        // After crawler will finished give folder with files to Lucene indexer
        try {
            Document doc = new Document();
            doc.add(new TextField("content", text, Field.Store.YES));
            doc.add(new StringField("title", title, Field.Store.YES));
            doc.add(new SortedDocValuesField("title", new BytesRef(title)));
            doc.add(new StringField("url", url, Field.Store.YES));
            writer.addDocument(doc);
            counter++;
            logger.info("Parsed: " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}