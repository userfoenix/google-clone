package com.userfoenix.services.lucene;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexNotFoundException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.userfoenix.domain.Page;

public class SearchEngineSearcher {

    private static StandardAnalyzer analyzer = new StandardAnalyzer();

    IndexSearcher indexSearcher;
    QueryParser queryParser;
    Query query;

    public SearchEngineSearcher(String indexDirectoryPath) throws IOException {
        Directory indexDirectory = FSDirectory.open(Paths.get(indexDirectoryPath));
        IndexReader reader;

        try {
            reader = DirectoryReader.open(indexDirectory);
        }
        catch (IndexNotFoundException e){
            // Init index
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            IndexWriter writer = new IndexWriter(indexDirectory, config);
            writer.close();
            System.out.println("!!!!!!!!!!!!!!!!!!!1");
            reader = DirectoryReader.open(indexDirectory);
            // ---
        }

        indexSearcher = new IndexSearcher(reader);

        queryParser = new QueryParser("content", analyzer);
    }

    public List<Page> search(String searchQuery, int limit, int offset, String order) {
        List<Page> items = new ArrayList<>();
        try {
            query = queryParser.parse(searchQuery);

            Sort sort = order.equals("abc") ? new Sort(new SortField("title", SortField.Type.STRING)) : Sort.RELEVANCE;

            TopDocs docs = indexSearcher.search(query, limit+offset, sort);
            ScoreDoc[] hits = docs.scoreDocs;
            SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter();
            Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));

            for (int i = 0, len = hits.length; i + offset < len; i++) {
                int docId = hits[i+offset].doc;
                Page page = new Page();
                Document d = indexSearcher.doc(docId);
                String content = d.get("content");
                String title = d.get("title");
                page.setUrl(d.get("url"));
                TokenStream tokenStream = analyzer.tokenStream("content", new StringReader(content));
                page.setContent(highlighter.getBestFragments(tokenStream, content, 3, "..."));

                tokenStream = analyzer.tokenStream("title", new StringReader(title));
                String hTitle = highlighter.getBestFragments(tokenStream, title, 1, "...");
                page.setTitle(hTitle.isEmpty() ? title : hTitle);

                items.add(page);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InvalidTokenOffsetsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return items;
    }
}