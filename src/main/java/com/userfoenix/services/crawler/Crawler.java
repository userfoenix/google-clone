package com.userfoenix.services.crawler;

import com.userfoenix.services.lucene.SearchEngineIndexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

public class Crawler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private HashMap<String, CrawlerTask> tasks;
    private int maxdeep;
    private int threadCount;
    private CrawlerCallback indexer;

    public Crawler(CrawlerCallback indexer, int maxdeep, int threadCount) {
        this.indexer = indexer;
        this.maxdeep = maxdeep;
        this.threadCount = threadCount;
        this.tasks = new HashMap<>();
    }

    public int getTaskId(String url) {
        return tasks.get(url).getTaskId();
    }

    public int getTaskStatus(String url) {
        return tasks.get(url).getStatus();
    }

    public int index(String url) throws MalformedURLException {
        tasks.put(url, new CrawlerTask(0));
        CrawlerAction.start(url, maxdeep, threadCount, indexer);
        tasks.get(url).setStatus(1);
        return tasks.get(url).getTaskId();
    }

    public static void main(String[] args) throws java.io.IOException {
        String url = "http://geekhub.ck.ua/";
        int maxdeep = 1;

        try {
            SearchEngineIndexer indexer = new SearchEngineIndexer("c:/tmp/");
            Crawler crawler = new Crawler(indexer, maxdeep,2);
            int taskId = crawler.index(url);
            indexer.close();
            System.out.println("taskId=" + taskId + " ,status=" + crawler.getTaskStatus(url));
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}

class CrawlerTask {
    public static int id = 0;

    private int status;
    private int taskId;

    CrawlerTask(int status) {
        this.taskId = ++CrawlerTask.id;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTaskId() {
        return taskId;
    }
}
