package com.userfoenix.services.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ConcurrentHashMap;

import java.net.*;


public class CrawlerAction extends RecursiveAction {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final ConcurrentHashMap visitedUrls = new ConcurrentHashMap();

    private String url;
    private int deep;
    private int maxdeep;
    private String prefix;
    private CrawlerCallback indexer;

    /**
     * Returns true if map not contained such url yet and false othrwise
     * @param url
     * @return Boolean
     */
    private static boolean addVisitedAndCheck(String url) {
        boolean isAdded = CrawlerAction.visitedUrls.put(url, 1) == null;
        return isAdded;
    }

    private static void clearVisited(){
        CrawlerAction.visitedUrls.clear();
    }

    public static void start(String url, int deep, int threadCount, CrawlerCallback indexer) throws MalformedURLException{
        CrawlerAction.clearVisited();
        ForkJoinPool pool = new ForkJoinPool(threadCount);
        pool.invoke(new CrawlerAction(url, 0, deep, indexer, ""));
    }

    private CrawlerAction(String url, int deep, int maxdeep, CrawlerCallback indexer, String prefix) throws MalformedURLException {
        this.url = url;
        this.deep = deep;
        this.maxdeep = maxdeep;
        this.indexer = indexer;
        if (prefix.isEmpty()){
            URL aURL = new URL(url);
            prefix = String.format("%s://%s", aURL.getProtocol(), aURL.getHost());
        }
        this.prefix = prefix;
    }

    @Override
    protected void compute() {
        List<CrawlerAction> subTasks = new LinkedList<>();
        try {
//            logger.info("Found: "+url);
            Document doc = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .ignoreHttpErrors(true)
                    .timeout(60 * 1000)
                    .get();
            if (deep < maxdeep) {
                Elements links = doc.select("a[href]");
                for (Element link : links) {
                    String href = link.attr("abs:href");
                    if (href.length()>0 && href.startsWith(this.prefix)) {
                        if (CrawlerAction.addVisitedAndCheck(href)) {
                            subTasks.add(new CrawlerAction(href, deep + 1, maxdeep, indexer, this.prefix));
                        }
                    }
                }
//                for (RecursiveAction subtask : subTasks) {
//                    subtask.fork();
//                    subtask.join();
//                }

            }
            indexer.process(url, doc.title(), doc.body().text());
            invokeAll(subTasks);
        } catch (Exception e) {
            //ignore 404, unknown protocol or other server errors
        }
    }
}