$(document).ready(()=>{
    console.log('Main js');
    setTimeout(()=>{
        $("#notice").hide();
    }, 5000);
});

function nextItems(q, sort='') {
    var offset = $('.items-holder').children().length;
    $.get('/ax_search', {q:q, offset:offset, sort:sort}, result => {
        var top = window.pageYOffset;
        var $btn = $("#next");
        $btn.button('loading');
        var html = result.reduce((acc, item)=>{
            return acc+`<div class="item">
<div class="title">
    <a href="${item.url}" target="_blank">${item.title}</a>
</div>
<div class="link">
    <a href="${item.url}" target="_blank">${item.url}</a>
</div>
    <div class="text">${item.content}</div>
</div>`;
        }, '');

        $('.items-holder').append(html);
        $btn.button('reset');
        window.scrollTo(0,top);
    })

}

function disableInputs(disabled=true) {
    // var $fields = $("select,input");
    var $btn = $("#start");
    if (disabled) {
        // $fields.attr("disabled", "disabled");
        $btn.button('loading');
    }
    else{
        // $fields.removeAttributeNode("disabled");
        $btn.button('reset');
    }
}